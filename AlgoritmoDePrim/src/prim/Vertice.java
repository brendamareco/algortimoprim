package prim;
import java.util.ArrayList;

public class Vertice
{
	
  private ArrayList<Integer> vecinos;
  private ArrayList<Double> aristas;
  private int verticeID, posicion;
  private boolean estado;
 
 	Vertice(int verticeNum, int posicion)
 	{
 		vecinos = new ArrayList<Integer>();
 		aristas = new ArrayList<Double>();
 		verticeID = verticeNum;
 		this.posicion = posicion;
 	}

	/*Al agregar un vecino implica que
	 * se agregue una arista adyacente
	 */
 	public void agregarVecino(int vecino, double peso)
 	{
 		verificarVecino(vecino);
 		verificiarArista(peso);
 		vecinos.add(vecino);
 		aristas.add(peso);
 	}
 	
 	private void verificiarArista(double peso) 
 	{
		if (peso <=0)
		{
			throw new IllegalArgumentException ("La arista debe tener peso >0");
		}
	}

	public boolean existeVecino(int vecino)
 	{
 		return vecinos.contains(vecino);
 	}
 	
 	// lanza una excepcion si el vecino existe o se quiere agregar un loop
	private void verificarVecino(int vecino) 
	{
		if(existeVecino(vecino) || verificarLoop(vecino))
			throw new IllegalArgumentException ("Error ! el vecino ya existe"); 
	}
	
	//TODO: cambiar por la pos, hagamos el vertice tenga una pos como identificador , (tal vez).
	private boolean verificarLoop(int vecino) 
	{
		if(this.verticeID == vecino)
			return true;
		else
			return false;
	}

	/*Al eliminar un vecino implica que
	 * se elimine la arista adyacente
	 */
	public void eliminarVecino(int vecino)
	{
		verificarVecinoInexistente( vecino );
		int pos_eliminar = vecinos.indexOf(vecino);
		vecinos.remove(pos_eliminar);
		aristas.remove(pos_eliminar);
	}
	
	//lanza una excepcion si el vecino no existe
	private void verificarVecinoInexistente(int vecino) 
	{
		if (!(existeVecino(vecino)))
			throw new IllegalArgumentException("Error ! el vecino no existe");
	}
	
	public double aristaMinima()
	{
		if (aristas.size()>0)
		{
			double posible_Minimo = aristas.get(0);
			for ( double arista: aristas)
			{
				if (arista <= posible_Minimo)
				{
					posible_Minimo = arista;
				}
			}
			return posible_Minimo;
		}
		//TODO: o retornar null?
		else throw new IllegalArgumentException ("Error! la lista de aristas esta vacia");
	}
	
	public int pos_Arista(double arista)
	{
		verificarAristaInexistente(arista);
		return aristas.indexOf(arista);
	}
	
	private boolean existeArista(double arista)
	{
		this.verificiarArista(arista);
		return aristas.contains(arista);
	}
	
	//lanza una excepcion si la arista no existe
	private void verificarAristaInexistente(double arista) 
	{
		if (!(existeArista(arista)))
		   throw new IllegalArgumentException ("Error ! la Arista no existe");	
		
	}
	
	public int pos_Vecino (int vecino)
	{
		verificarVecinoInexistente(vecino);
		return vecinos.indexOf(vecino);
	}
	
	public double get_IesimaArista(int posicion)
	{
		validarIndiceArista (posicion);
		return aristas.get(posicion);	
	}
	
	private void validarIndiceArista(int posicion)
	{
		if (posicion >= aristas.size())
			throw new IllegalArgumentException ("Error ! indice de aristas invalido");
	}

	public int get_iesimoVecino(int posicion)
	{
		validarIndiceVecino(posicion);
		return vecinos.get(posicion);
	}

	private void validarIndiceVecino(int posicion) 
	{
		if(posicion >= aristas.size())
			throw new IllegalArgumentException ("Error ! indice de vecinos invalido");
	}
	
	public int BuscarVecino_AtravesDeArista(double arista)
	{
		this.verificarAristaInexistente(arista);
		int pos_arista = this.pos_Arista(arista);
		return this.get_iesimoVecino(pos_arista);
	}
	
	public double BuscarArista_AtravesDeVecino(int vecino)
	{
		this.verificarVecinoInexistente(vecino);
		int pos_vecino = this.pos_Vecino(vecino);
		return this.get_IesimaArista(pos_vecino);
	}

	public ArrayList<Integer> getVecinos() 
	{
		return vecinos;
	}

	public ArrayList<Double> getAristas()
	{
		return aristas;
	}

	public int getVerticeID() 
	{
		return verticeID;
	}

	public void setVerticeID(int verticeID) 
	{
		this.verticeID = verticeID;
	}

	public int getPosicion() 
	{
		return posicion;
	}

	public void setPosicion(int posicion) 
	{
		this.posicion = posicion;
	}

	public boolean estado() 
	{
		return estado;
	}

	public void setEstado(boolean estado)
	{
		this.estado = estado;
	}
	
	@Override
	public boolean equals(Object o)
	{
		boolean resultado = true;
		if (o == null && resultado == false )
		{
			return false;
		}
		else
		{
			if (!(o instanceof Vertice))
			{ 
				return false;
			}
			else
			{
				//castear object o a tipo Vertice;
				Vertice comparar = (Vertice) o;
				resultado = resultado && this.vecinos.equals(comparar.vecinos);
				resultado = resultado && this.aristas.equals(comparar.aristas);
				resultado = resultado && this.posicion == comparar.posicion;
				resultado = resultado && this.verticeID == comparar.verticeID;	
			}
		}
		return resultado;
	}
}
