package prim;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class VerticeTest 
{
	@Test(expected = IllegalArgumentException.class)
	public void agregarVecinoExistente()
	{
		Vertice a= new Vertice(2,10);
		Vertice b = new Vertice (5,20);
		
		a.agregarVecino(b.getVerticeID(), 4);
		a.agregarVecino(b.getVerticeID(), 5);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void agregarBucles()
	{
		Vertice a= new Vertice(2,10);
		
		a.agregarVecino(a.getVerticeID(), 4);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void eliminarVecinoInexistente()
	{
		Vertice a= new Vertice(2,10);
		a.eliminarVecino(3);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void agregarAristaNegativa()
	{
		Vertice a= new Vertice(2,10);
		Vertice b = new Vertice (5,20);
		
		a.agregarVecino(b.getVerticeID(), -4);
	}
	
	@Test
	public void compararVecinos()
	{
	
		Vertice a = new Vertice (1,10);
		Vertice b = new Vertice (2,230);
		Vertice c = new Vertice (3,110);
		Vertice d = new Vertice (4,220);
		
		a.agregarVecino(b.getVerticeID(), 3);
		a.agregarVecino(c.getVerticeID(), 3);
		a.agregarVecino(d.getVerticeID(), 3);
		
		ArrayList<Integer> vecinos= new ArrayList<>();
		vecinos.add(2);
		vecinos.add(3);
		vecinos.add(4);
		
		assertTrue(vecinos.containsAll(a.getVecinos()));
	}
	
}
