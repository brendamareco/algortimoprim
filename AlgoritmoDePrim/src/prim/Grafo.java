package prim;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Grafo 
{
	// Representamos un grafo por listas de vecinos
		private ArrayList<Vertice> vecinos;

		// La cantidad de v�rtices queda fija desde la construcci�n
		public Grafo()
		{
			vecinos = new ArrayList<>();
		}
		
		// Cantidad de v�rtices
		public int vertices()
		{
			return vecinos.size();
		}
		
		
		// Agregar una arista
		public void agregarArista(Vertice origen, Vertice vecino, double arista)
		{
			verificarVerticeValidos(origen,vecino);
			//TODO: preguntar 
			// si no existe origen o vecino los agregamos al grafo
			//sabiendo que cumplen con las condiciones para que se agreguen
			if(!(existeVertice(origen)))
				this.agregarVertice(origen);
			if(!(existeVertice(vecino)))
				this.agregarVertice(vecino);
			verificarAristaExistente(origen.getVerticeID(), vecino.getVerticeID());
			//agregamos la arista a los vertices origen y destino
			for (Vertice vertice : vecinos)
			{
				if(vertice.equals(origen))
					vertice.agregarVecino(vecino.getVerticeID(), arista);
				else 
					if (vertice.equals(vecino))
					vertice.agregarVecino(origen.getVerticeID(), arista);
			}
		}
		
		private void verificarVerticeValidos(Vertice origen, Vertice vecino)
		{
			//los vertices no pueden tener un mismo ID
			if(origen.getVerticeID()== vecino.getVerticeID())
				throw new IllegalArgumentException("No se pueden agregar loops! V�rticeID = "
						+ origen.getVerticeID() );
			
			//los vertices no pueden tener la misma posicion
			if(origen.getPosicion()== vecino.getPosicion())
				throw new IllegalArgumentException("Los vertices deben tenes posiciones diferentes");
		}
		
		public void agregarVertice(Vertice vertice)
		{
			verificarVerticeExistente(vertice);
			vecinos.add(vertice);
		}
		
		//consulata si existe un vertice
		private boolean existeVertice(Vertice vertice)
		{
			return vecinos.contains(vertice);
		}
		
		//si existe el vertice lanzo una excepcion
		private void verificarVerticeExistente(Vertice vertice) 
		{
			if(existeVertice(vertice))
				throw new IllegalArgumentException("El vertice ingresado ya existe ");
		}
		
		//si no existe el vertice lanzo una excepcion
		private void verificarVerticeInExistente(Vertice vertice) 
		{
			if(!(existeVertice(vertice)))
				throw new IllegalArgumentException("El vertice ingresado no existe ");
		}

		// Consulta si existe una arista
		public boolean existeArista(int verticeID, int vecinoID)
		{
			boolean resultado = true;
			for( Vertice vertice : vecinos)
			{
				// la lista de vecinos de Vertice es simetrica es decir que si dos Vertices son vecinos
				//entonces cada uno esta en la lista de vecinos del otro
				//por lo tanto solo basta con recorrer un Vertice y preguntar si contiene al otro
				if( vertice.getVerticeID()== verticeID)
					resultado = resultado &&	vertice.existeVecino(verticeID);				
			}			
			return resultado;
		}
		
		// Lanza una excepci�n si la arista existe
		private void verificarAristaExistente(int origenID, int vecinoID)
		{
			if( existeArista(origenID, vecinoID) )
				throw new IllegalArgumentException("Arista existente! (i,j) = (" + origenID + "," + vecinoID +")");
		}

		// Lanza una excepci�n si la arista no existe
		private void verificarAristaInexistente(int origenID, int vecinoID)
		{
			if( existeArista(origenID, vecinoID) == false )
				throw new IllegalArgumentException("Arista inexistente! (i,j) = (" + origenID + "," + vecinoID +")");
		}

		// Elimina una arista
		public void eliminarArista(Vertice vertice, Vertice vecino)
		{
			int pedido= vertice.getVerticeID();
			int eliminar= vecino.getVerticeID();
			//verificamos que se pueda eliminar la arista
			this.verificarVerticeInExistente(vertice);
			this.verificarVerticeInExistente(vecino);
			verificarAristaInexistente(pedido , eliminar);
			
			Vertice verticeOrigen = buscarVertice(vertice);
			Vertice verticeVecino = buscarVertice(vecino);
			//eliminar el vertice vecino implica eliminar la arista adyacente a ellos 
			verticeOrigen.eliminarVecino(eliminar);
			verticeVecino.eliminarVecino(pedido);
		}
		
		private Vertice buscarVertice(Vertice vertice)
		{
			for (Vertice vert : vecinos)
			{
				if (vert.equals(vertice))
				return vert;
			}
			throw new IllegalArgumentException("El vertice no existe en el grafo");
		}
		
		public Vertice buscarVertice(int verticeID)
		{
			for (Vertice vertice : vecinos)
			{
				if (vertice.getVerticeID()== verticeID)
				{
					return vertice;
				}
				
			}
			throw new IllegalArgumentException ("No hay ningun vertice en el grafo con el numero : "+ verticeID);
		}
		
		public ArrayList<Vertice> get_Vertices()
		{
			return this.vecinos;
		}
		
		// Vecinos de un v�rtice
		public ArrayList<Integer> vecinosDeUnVertice(Vertice vertice)
		{
			verificarVerticeExistente(vertice);
			Vertice pedido = buscarVertice(vertice);
			return pedido.getVecinos();
		}	
		
}
