package prim;

import java.util.ArrayList;

public class Arbol
{
	public Nodo raiz;
	/*Utilizamos la clase  Vertices para representar cada nodo, 
	 * ya que las aristas van a poseer peso
	 * ademas necesitamos crear la clase Nodo
	 * para que los vertices tengan los punteros a sus "vecinos" que seran 
	 * los nodos que estaran debajo del vertice
	 */
	public class Nodo
	{
		private Vertice vertice; 
		private ArrayList <Nodo> hijos;
		
		public Nodo (Vertice vertice)
		{
			this.vertice = vertice;
			hijos= new ArrayList<>();
		}
	}
	
	
	public Arbol()
	{
		raiz = null;
	}
	
	public void agregarRaiz(Vertice padre) 
	{
		Nodo nodo = new Nodo(padre);
		if(raiz==null)
			raiz = nodo;
		
	}

	/* otorgamos dos vertices para que sean agregados,
	 * si y solo si no existe relacion entre ellos 
	 * notar que el primer vertice va a ser el padre del segundo
	 * 
	 */
	public void agregarHijo_a_Nodo(Vertice vertice, Vertice vecino)
	{
		
		if(raiz == null)
			agregarRaiz(vertice);
		
		
		if (parametrosCorrectos(vertice,vecino)) 
		{
			//busco en el arbol que tengo al nodo que contenga mi vertice
			Nodo nodoPadre = buscarNodo(vertice);
			Nodo nodoHijo = new Nodo (vecino);
			nodoPadre.hijos.add(nodoHijo);
		}
		else
			throw new IllegalArgumentException("Parametros incorrectos: el nodo "
					+ vertice +" no existe o ya existe su hijo "+ vecino );
	}

	private boolean parametrosCorrectos(Vertice vertice, Vertice vecino) 
	{
		if ( buscarNodo(vertice)==null || existeRelacion(vertice,vecino))
			return false;
		else
			return true;
	}

	private boolean existeRelacion(Vertice vertice, Vertice vecino) 
	{
		Nodo padre = buscarNodo (vertice);
		Nodo hijo = new Nodo(vecino);
		if (padre != null) 
		{
			if (padre.hijos.contains(hijo))
				return true;
			else 
				return false;
		}
			return false;
	}

	private Nodo buscarNodo(Vertice vertice) 
	{
		if (raiz.vertice.equals(vertice))
		return raiz;
		else
			return buscarNodo(raiz,vertice);
	}

	private Nodo buscarNodo(Nodo nodo, Vertice vertice) 
	{
		for(Nodo hijo : nodo.hijos)
		{
			if(hijo.vertice.equals(vertice))
				return hijo;
			else
				return buscarNodo(hijo,vertice);
		}
		
		return null;
	}
	
	@Override
	public String toString()
	{ 
	if(!(raiz==null))
	{
		return toString(raiz," "); 
		
	}
	throw new IllegalArgumentException("La raiz es null");
		
	}
	public ArrayList<Integer> getVecinosDeunNodo(Vertice aBuscar)
	{
		//busca el vertice que le pasamos en nuestro arbol
		Nodo busqueda = this.buscarNodo(aBuscar);
		if (busqueda!= null)
		{
			// retorna la lista de vecinos del vertice, si es que existe
			return busqueda.vertice.getVecinos();
		}
		throw new IllegalArgumentException ("El nodo pedido no se encuentra en el arbol");
	}

	private String toString(Nodo nodo, String ret)
	{ 
		ret = ret +"Nodo ID = " + nodo.vertice.getVerticeID() + "\r\n";
		//si mi nodo tiene hijos
		if(nodo.hijos.isEmpty()==false)
			{
				//imprimo informacion de los hijos de un vertice
				for (Nodo hijos : nodo.hijos)
				{
					ret= ret + " -- apunta al vertice - > " + hijos.vertice.getVerticeID() +" ";
					double aristaPeso = hijos.vertice.BuscarArista_AtravesDeVecino(nodo.vertice.getVerticeID());
					ret = ret+ "con una arista de :" + aristaPeso + "\r\n";
				}
				//TODO: no funciona , pensar error!, no imprime hijos de los nodos
				for (int i=0; i < nodo.hijos.size();i++)
				{
					return toString(nodo.hijos.get(i), ret);
				}
				
			}
		
		
		return ret;
	}
	
}
