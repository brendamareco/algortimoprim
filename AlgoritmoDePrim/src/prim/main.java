package prim;

public class main {

	public static void main(String[] args)
	{
		Vertice a = new Vertice(2,10);
		Vertice b = new Vertice(3,10);
		Vertice c = new Vertice(4,10);
		Vertice d = new Vertice(5,10);
		
		a.agregarVecino(b.getVerticeID(),5);
		a.agregarVecino(c.getVerticeID(), 2);
		c.agregarVecino(a.getVerticeID(), 2);
		b.agregarVecino(a.getVerticeID(),5);
		b.agregarVecino(d.getVerticeID(),2);
		d.agregarVecino(b.getVerticeID(),2);
		
		Arbol arb= new Arbol();
		arb.agregarHijo_a_Nodo(a, b);
		arb.agregarHijo_a_Nodo(a, c);
		arb.agregarHijo_a_Nodo(b, d);
	System.out.println(arb.toString());	
	
	}

}
