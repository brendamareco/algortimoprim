package prim;

import java.util.ArrayList;

public class AlgortimoDePrim 
{
	public static Arbol prim_AGM(Grafo g) 
	{
		Arbol AGM = new Arbol();
		ArrayList<Vertice> vertices = g.get_Vertices();
		ArrayList<Vertice> verticesVisitados= new ArrayList<Vertice>();
		verticesVisitados.add(vertices.get(0));
		int i =1;
		while(i <= vertices.size()-1)
		{
			Vertice seleccionado = null;
			Vertice vecino = null;
			int vecinoID = 0;
			double posibleMinimo = Double.POSITIVE_INFINITY;
			for(Vertice vert : verticesVisitados )
			{
				double aristaMinimaDelVertice = vert.aristaMinima();
				int apuntado = vert.BuscarVecino_AtravesDeArista(aristaMinimaDelVertice);
				Vertice verticeApuntado = g.buscarVertice(apuntado);
				
				if (!(verticesVisitados.contains(verticeApuntado)) && aristaMinimaDelVertice <= posibleMinimo) 
				{
					posibleMinimo = aristaMinimaDelVertice;
					seleccionado =vert;
					vecinoID = apuntado;
					vecino = verticeApuntado;
				}
			}
			//elimino el vecino del seleccionado
			seleccionado.eliminarVecino(vecinoID);
			int pos = vertices.indexOf(vecino);
		    // elimino el ID del seleccionado del vecino
			verticesVisitados.get(pos).eliminarVecino(seleccionado.getVerticeID());
			// me creo los nuevos vertices que voy a agregar al arbol
			Vertice agregarPadre= new Vertice(seleccionado.getVerticeID(), seleccionado.getPosicion());
			agregarPadre.agregarVecino(vecinoID, posibleMinimo);
			Vertice agregarHijo = new Vertice (vecino.getVerticeID(),vecino.getVerticeID());
			agregarHijo.agregarVecino(seleccionado.getVerticeID(), posibleMinimo);
			//agrego los vertices creados a mi arbol
			AGM.agregarHijo_a_Nodo(agregarPadre, agregarHijo);
			i++;
		}
		
		return AGM;
	}
}
